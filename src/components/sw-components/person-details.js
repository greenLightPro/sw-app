import ItemDetails, {Record} from "../item-details";
import { withSwapiService } from "../hoc-helpers";


const PersonDetails = (props) => {
    return (
        <ItemDetails { ... props }
        >
            <Record field='name' label='Name' />
            <Record field='birthYear' label='Birth' />
        </ItemDetails>
    );
};

const mapMethodsToProps = (swapiService) => {
    return {
        getData: swapiService.getPerson,
        getImageUrl: swapiService.getPersonImage,
    };
}



export default withSwapiService(mapMethodsToProps)(PersonDetails);
