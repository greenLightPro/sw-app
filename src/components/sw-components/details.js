import ItemDetails, {Record} from "../item-details";
import { SwapiServiceConsumer } from "../swapi-service-context";


const PersonDetails = ({ itemId }) => {
    return (
        <SwapiServiceConsumer>
            {
                ({ getAllPeople, getPersonImage }) => {
                    return (
                        <ItemDetails itemId={itemId}
                                     getData={getAllPeople}
                                     getImageUrl={getPersonImage}
                        >
                            <Record field='name' label='Name' />
                            <Record field='birthYear' label='Birth' />
                        </ItemDetails>
                    )
                }
            }
        </SwapiServiceConsumer>

    );
};

const StarshipDetails = ({ itemId }) => {
    return (
        <SwapiServiceConsumer>
            {
                ( {getAllStarships, getStarshipImage} ) => {
                    return (
                        <ItemDetails itemId={itemId}
                                     getData={getAllStarships}
                                     getImageUrl={getStarshipImage}
                        >
                            <Record field='name' label='Name' />
                            <Record field='model' label='Model' />
                        </ItemDetails>
                    );
                }
            }
        </SwapiServiceConsumer>
        );
};

const PlanetDetails = ({ itemId }) => {
    return (
        <SwapiServiceConsumer>
            {
                ( {getAllPlanets, getPlanetImage} ) => {
                    return (
                        <ItemDetails itemId={itemId}
                                     getData={getAllPlanets}
                                     getImageUrl={getPlanetImage}
                        >
                            <Record field='name' label='Name' />
                            <Record field='periodRotation' label='Period rotation' />
                        </ItemDetails>
                    );
                }
            }
        </SwapiServiceConsumer>

    );
};

export {
    PersonDetails,
    StarshipDetails,
    PlanetDetails,
};
