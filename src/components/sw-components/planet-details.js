import ItemDetails, {Record} from "../item-details";
import { withSwapiService } from "../hoc-helpers";

const PlanetDetails = ( props) => {

    return (
        <ItemDetails { ... props } >
            <Record field='name' label='Name' />
            <Record field='rotationPeriod' label='Period rotation' />
        </ItemDetails>
    );
};

const mapMethodsToProps = (swapiService) => {
    return {
        getData: swapiService.getPlanet,
        getImageUrl: swapiService.getPlanetImage,

    }
}

export default withSwapiService(mapMethodsToProps)(PlanetDetails);