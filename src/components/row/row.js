import React from "react";

const Row = ( {left, right} ) => {
    return (
        <div className="sw-items-box mt-4 row g-3">
            <div className="col-6">
                {left}
            </div>
            <div className="col-6">
                {right}
            </div>
        </div>
    )
};

export default Row;