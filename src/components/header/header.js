import React from "react";
import "./header.css";
import { Link } from "react-router-dom";

const Header = (props) => {
    return (
        <div className="sw-app-header py-3 d-flex">
            <Link to="/" className="logo sw-logo me-4">SW-App</Link>

            <ul className="nav sw-nav">
                <li className="nav-item sw-nav-item">
                    <Link to="/people/" className="nav-link link-dark">People</Link>
                </li>
                <li className="nav-item sw-nav-item">
                    <Link to="/planets/" className="nav-link link-secondary">Planets</Link>
                </li>
                <li className="nav-item sw-nav-item">
                    <Link to="/starships/" className="nav-link link-secondary">Starships</Link>
                </li>
            </ul>
        </div>
    )
}

export default Header;