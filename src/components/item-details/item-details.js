import React, {Component} from 'react';
import './item-details.css';
import Spinner from "../spinner";
import ErrorIndicator from "../error-indicator";


const Record = ({ item, field, label }) => {
    return (
        <li className="list-group-item">
            <span className="text-secondary">{ label }:</span>  <span>{ item[field] }</span>
           </li>
    );
};
export { Record } ;

export default class ItemDetails extends Component {

    swapiService = this.props.swapiService

    state = {
        item: null,
        image: null,
        loading:false,
        error: false
    }

    updateItem() {

        const { itemId, getImageUrl, getData } = this.props;

        if (!itemId) {
            return;
        }

        this.setState({
            loading:true,
            error: false,
        });


        getData(itemId)
            .then(item => {
                this.setState({
                    item: item,
                    image: getImageUrl(itemId),
                    loading:false,
                });
            })
            .catch((error) => {
                this.setState({
                    loading:false,
                    error: true,
                });
            });

    }

    componentDidMount() {
        this.updateItem();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.itemId !== prevProps.itemId) {
            this.updateItem();
        }
    }

    render() {

        const { item, image, loading, error } = this.state;

        if ( !item && !loading ) {
            return (
                <p>Select any item from the list</p>
            );
        }

        if ( !item && loading ) {
            return (
                <Spinner/>
            );
        }

        const { name } = item;

        const errorIndicator = error ? <ErrorIndicator/> : null;

        const itemContent = ! (loading || error) ?  (
                <div className="sw-unit">
                    <div className="sw-unit__image rounded">
                        {
                            image ?  (<img src={image} alt={name}/>) : ''
                        }
                    </div>
                    <div className="sw-unit__content ms-3">
                        <h3 className="mb-3">{name}</h3>

                        <ul className="list-group">
                            {
                                React.Children.map(this.props.children, (child) => {
                                   return React.cloneElement(child, {item});
                                })
                            }

                        </ul>
                    </div>
                </div>
        ) : null;

        const spinner = loading ? <Spinner/> : null;


        return (
            <div className="sw-items-box__content">
                { errorIndicator }
                { spinner }
                { itemContent }
            </div>
        );
    }

}


