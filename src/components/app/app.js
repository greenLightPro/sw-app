import React, {Component} from 'react';
import './app.css';
import Header from '../header';
import RandomPlanet from '../random-planet';
import { SwapiServiceProvider } from '../swapi-service-context';
import SwapiService from "../../services/swapi-service";

import { PeoplePage, PlanetsPage, StarshipsPage, PageNoUrl } from "../pages";
import {BrowserRouter as Router, Route, Switch, Redirect} from "react-router-dom";
import {  PersonDetails } from "../sw-components";


export default class App extends Component  {
    swapiService = new SwapiService();

    render() {
        return (
            <SwapiServiceProvider value={ this.swapiService }>
                <div className="sw-app-block mt-3">
                    <div className="container-fluid">
                        <div className="sw-app">
                            <Router>
                                <Header />
                                <RandomPlanet updateInterval={600000} />

                                <Switch>
                                    <Route path="/" exact render={()=><h3 className="my-3">Главная</h3>} />
                                    <Route path="/people/"  exact component={PeoplePage} />
                                    <Route path="/people/:id"  exact render={
                                        ({ match }) => {
                                            const {id} = match.params;
                                            return (
                                                <div className="bg-white border rounded shadow-sm p-3">
                                                    <PersonDetails  itemId = {id} />
                                                </div>


                                            );
                                        }
                                    } />
                                    <Route path="/planets/:id?" exact component={ PlanetsPage } />
                                    <Route path="/starships/" exact component={ StarshipsPage} />

                                    {/*<Redirect to="/" />*/}

                                    <Route component={ PageNoUrl } />
                                </Switch>


                            </Router>
                        </div>
                    </div>
                </div>
            </SwapiServiceProvider>

        );
    }
};





