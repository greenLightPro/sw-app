import React, {Component} from 'react';
import './random-planet.css';
import SwapiService from "../../services/swapi-service";
import Spinner from "../spinner";
import ErrorIndicator from "../error-indicator";
import PropTypes from 'prop-types';

export default class RandomPlanet extends Component {

    state = {
        planet: {},
        loading: true,
        error: false,
    }

    swapiService = new SwapiService();

    onPlanetLoaded = (planet) => {
        this.setState({
            planet,
            loading: false,
        });
    }

    onError = (error) => {
        this.setState({
            error: true,
            loading:false,
        })
    }

    updatePlanet = () => {
        const id = Math.floor(Math.random() * 25 + 3);
        this.swapiService
            .getPlanet(id)
            .then((planet) => {
                this.onPlanetLoaded(planet);
            })
            .catch(this.onError);
    }

    componentDidMount() {
        const { updateInterval } = this.props;
        this.updatePlanet();
        this.interval = setInterval(this.updatePlanet, updateInterval);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {

        const {loading, planet, error} = this.state;
        const hasData = !(loading || error);
        const errorMessage = error ? <ErrorIndicator /> : null;
        const spinner = loading ? <Spinner /> : null;
        const content = hasData ? <PlanetView planet={ planet } /> : null;

        return (
            <div className="sw-random-box bg-white border rounded shadow-sm mt-2 mb-3 p-3">
                { errorMessage }
                { spinner }
                { content }
            </div>
        );
    }

}

RandomPlanet.defaultProps = {
    updateInterval: 100000,
};
RandomPlanet.propTypes = {
    /*
    updateInterval: (props, propName, componentName) => {
        const value = props[propName];
        if(typeof value === 'number' &&  !isNaN(value)) {
            return null;
        }
        return new TypeError(`${componentName} : ${propName} must be number`);
    },
     */
    updateInterval: PropTypes.number,
};

const PlanetView = ({ planet }) => {
    const { id, name, population, rotationPeriod, diameter } = planet;

    return (
        <React.Fragment>
            <div className="sw-random__image me-4 rounded">
                <img src={`https://starwars-visualguide.com/assets/img/planets/${id}.jpg`} alt={name} />
            </div>
            <div className="sw-random__content">
                <h3 className="mb-3">{ name }</h3>
                <ul className="list-group">
                    <li className="list-group-item">Population: { population }</li>
                    <li className="list-group-item">Rotation: { rotationPeriod }</li>
                    <li className="list-group-item">Diameter: { diameter }</li>
                </ul>
            </div>
        </React.Fragment>
        );
}
