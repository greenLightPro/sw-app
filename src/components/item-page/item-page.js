import React, { Component } from 'react';
import './item-page.css';
import ErrorIndicator from "../error-indicator";
import Row from "../row";
import ErrorBoundry from "../error-boundry/error-boundry";

import {
    PersonList,
    PersonDetails,
} from "../sw-components";

export default class ItemPage extends Component {

    state = {
        selectedItem: null,
    }

    onItemSelected = (id) => {
        this.setState({
            selectedItem: id,
        })
    }

    render() {
        const {selectedItem, error} = this.state;

        if (error) {
            return (
                <ErrorIndicator/>
            );
        }

        const leftComponent = (
            <PersonList onItemSelected = {this.onItemSelected} />
        );

        const rightComponent = (
            <PersonDetails itemId={ selectedItem } />
        );

        return (
            <ErrorBoundry>
                <Row left={leftComponent} right={rightComponent}/>
            </ErrorBoundry>
        );
    }
}

