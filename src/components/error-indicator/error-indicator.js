import React from 'react';
import './error-indicator.css';
import icon from './error-icon.png';

const ErrorIndicator = () => {
    return (
        <div className="alert alert-danger error-indicator">
            <img src={icon} alt=""/>
            <p>Something went wrong, but we already try to fix it</p>
        </div>
    )
}

export default ErrorIndicator;