import React from 'react';
import './item-list.css';

const ItemList = (props) => {

    const { data, onItemSelected, children: renderLabel  } = props;

    let items;

    if (data !== null) {
        items =  data.map((item) => {
            const { id } = item;
            const label = renderLabel(item);
            return (
                <li className="list-group-item list-group-item-action"
                    key={id}
                    onClick={() => onItemSelected(id)}
                ><span>{ label }</span></li>
            );
        });
    } else {
        items = null;
    }

    return (
        <div className="sw-items-box__list">
            { items }
        </div>
    );
}

export default ItemList;



