import React  from 'react';

import Row from "../row";

import { PlanetList, PlanetDetails } from "../sw-components";
import { withRouter } from "react-router-dom";

const PlanetsPage = ( {history, match} ) => {

    const { id } = match.params;

    const leftComponent = (
        <PlanetList onItemSelected = { (id) => history.push(id) } />
    );

    const rightComponent = (
        <PlanetDetails itemId={ id } />
    );
    return (
        <Row left={leftComponent} right={rightComponent}/>
    )

};

export default withRouter(PlanetsPage);
