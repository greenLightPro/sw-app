import React from 'react';
import {Link} from "react-router-dom";

const PageNoUrl = () => {
    return (
        <div>
            <h3 className="mb-2">No such page</h3>
            <p>Go to <Link className="link-secondary" to="/">main</Link></p>
        </div>
    )
}

export default PageNoUrl;