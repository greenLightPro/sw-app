import React, { Component } from 'react';
import ErrorIndicator from "../error-indicator";
import Row from "../row";
import ErrorBoundry from "../error-boundry/error-boundry";

import { StarshipList, StarshipDetails } from "../sw-components";

export default class StarshipsPage extends Component {

    state = {
        selectedItem: null,
    }

    onItemSelected = (id) => {
        this.setState({
            selectedItem: id,
        })
    }

    render() {
        const {selectedItem, error} = this.state;

        if (error) {
            return (
                <ErrorIndicator/>
            );
        }

        const leftComponent = (
            <StarshipList onItemSelected = {this.onItemSelected} />
        );

        const rightComponent = (
            <StarshipDetails itemId={ selectedItem } />
        );

        return (
            <ErrorBoundry>
                <Row left={leftComponent} right={rightComponent}/>
            </ErrorBoundry>
        );
    }
}
