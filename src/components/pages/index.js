import PeoplePage from "./people-page";
import PlanetsPage from "./planets-page";
import StarshipsPage from "./starships-page";
import PageNoUrl from "./page-no-url";


export {PeoplePage, PlanetsPage, StarshipsPage, PageNoUrl};