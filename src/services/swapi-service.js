
export default class SwapiService {

    _apiBase = 'https://swapi.dev/api';
    _imgBase = 'https://starwars-visualguide.com/assets/img';



    async getResource(url) {
        const res = await fetch(`${this._apiBase}${url}`);
        if ( !res.ok ) {
            throw Error(`Couldn't fetch ${url}, received ${res.status}`);
        }
        return await res.json();
    }

     getAllPeople = async () => {
        const res = await this.getResource(`/people/`);
        return res.results.map(this._transformPerson);
    }

    getPerson = async (id) => {
        const person = await this.getResource(`/people/${id}/`);
        return this._transformPerson(person);
    }

    getAllStarships = async () => {
        const res = await this.getResource(`/starships/`);
        return res.results.map(this._transformStarship);
    }

    getStarship = async (id) => {
        const starship = await this.getResource(`/starships/${id}/`);
        return this._transformStarship(starship);
    }

    getAllPlanets = async () => {
        const res = await this.getResource(`/planets/`);
        return res.results.map(this._transformPlanet);
    }

    getPlanet = async (id) => {
        const planet  = await this.getResource(`/planets/${id}/`);
        return this._transformPlanet(planet);
    }

    getPlanetImage = (id) => {
        return `${this._imgBase}/planets/${id}.jpg`;
    }
    getPersonImage = (id) => {
        return `${this._imgBase}/characters/${id}.jpg`;
    }
    getStarshipImage = (id) => {
        return `${this._imgBase}/starships/${id}.jpg`;
    }

    _extractId(item) {
        const idRegExp = /\/([0-9]+)\/$/;
        return item.url.match(idRegExp)[1];
    }

    _transformPlanet = (planet) => {
        const id = this._extractId(planet);

        return {
             id,
            name: planet.name,
            population: planet.population,
            rotationPeriod: planet.rotation_period,
            diameter: planet.diameter,
        }
    }

    _transformStarship = (starship) => {
        const id = this._extractId(starship);
        return {
            id,
            name: starship.name,
            model: starship.model,
            manufacturer: starship.manufacturer,
            costInCredits: starship.cost_in_credits,
            length: starship.length,
            crew: starship.crew,
            passengers: starship.passengers,
            cargoCapacity: starship.cargo_capacity,
        }
    }

    _transformPerson = (person) => {
        const id = this._extractId(person);

        return {
            id,
            name: person.name,
            birthYear: person.birth_year,
            gender: person.gender,
            height: person.height,
            mass: person.mass,
            eyeColor: person.eye_color,
            hairColor: person.hair_color,
            skinColor: person.skin_color
        }
    }

}




